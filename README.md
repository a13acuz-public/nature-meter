# nature-meter
Digital meter system which store "Nature Remo" data and show temperature, humidity and comfortability in the "LaMetric Time".

## Dependency
* Nature Remo
* LaMetric Time
  * Any private LaMetic Application which gets information from outside server(=Communication type: "Push") is needed
* Python (==3.8)
* Serverless (==2.15.0)
* aws-cli (>=1.18.49)

## Setup
1. Create python virtual environment
    ```bash
    python3.8 -m venv venv
    ```

2. Install plugins for serverless
    ```bash
    npm install
    ```

3. Deploy system to AWS
    ```bash
    export STAGE=<Environment Name, e.g. "prod">
    export EMAIL=<email address for alerting>

    sls deploy -v --stage ${STAGE} --email ${EMAIL}
    ```

4. Add secret information
   * Add "Nature Remo" secret
        ```bash
        export STAGE=<Environment name defined above>
        export DEVICE_ID=<ID of Nature Remo from which this system fetch data>
        export TOKEN=<API Token of Nature Remo to fetch data>

        aws secretsmanager update-secret \
        --secret-id naturemeter-${STAGE}-NatureRemo \
        --secret-string '{"DEVICE_ID":"${DEVICE_ID}","TOKEN":"${TOKEN}"}'
        ```

   * Add "LaMetic Time" secret
        ```bash
        export STAGE=<Environment name defined above>
        export TOKEN=<API Token for sending request to LaMetric Time>
        export URL=<Endpoint of LaMetric Time which show data>

        aws secretsmanager update-secret \
        --secret-id naturemeter-${STAGE}-LaMetricTime \
        --secret-string '{"TOKEN":"${TOKEN}","URL":"${URL}"}'
        ```

## Note
* If you want to test Lambda function in the local, use following command.
    ```bash
    sls invoke local -f DataTransfer
    ```

## Author
abacus

## References
* [AWS LambdaとServerless Frameworkで爆速で作るTwitterbot](https://qiita.com/gkzz/items/23a7d03aeadcec341700)
