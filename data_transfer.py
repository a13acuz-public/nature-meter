from botocore.exceptions import ClientError
from botocore.config import Config
from typing import Dict, List, Tuple
from datetime import datetime
import time
import requests
import boto3
import json
import sys
import os


def _get_secrets(secret_id: str) -> Dict:
    try:
        client = boto3.client(service_name="secretsmanager")
        result = client.get_secret_value(SecretId=secret_id)
    except ClientError as e:
        raise e
    else:
        secret = json.loads(result["SecretString"])
        return secret


def _get_device_data(token: str, device_id: str) -> Dict:
    headers = {
        "accept": "application/json",
        "Authorization": "Bearer " + token
    }

    response = requests.get(
        "https://api.nature.global/1/devices",
        headers=headers
    )
    response = response.json()

    for r in response:
        if r["id"] == device_id:
            return r
        else:
            return None


def _extract_environment_data(device_data: Dict) -> Dict:
    raw_data = device_data["newest_events"]
    env_data = {
        "device_id": device_data["id"],
        "timestamp": str(int(round(time.time()))),
        "humidity": raw_data["hu"]["val"],
        "illuminance": raw_data["il"]["val"],
        # Use "created_at" data to detect motion because "val" of motion is always "1"(Ref: https://zlog.hateblo.jp/entry/2019/07/07/Nature-remo-motion-sensor).
        # Replace "Z" to "+00:00" in order to avoid errors of function(Ref: https://kokufu.blogspot.com/2018/12/python-datetime-unix-time.html).
        "motion":  int(datetime.fromisoformat(raw_data["mo"]["created_at"].replace("Z", "+00:00")).timestamp()),
        "temperature": raw_data["te"]["val"]
    }

    return env_data


def _post_environment_data(token: str, url: str, env_data: Dict):
    headers = {
        "Accept": "application/json",
        "X-Access-Token": token,
        "Cache-Control": "no-cache"
    }

    data = {
        "frames": [
            {
                "text": " {}C°{}%".format(int(env_data["temperature"]),
                                          env_data["humidity"]),
                # Change icon based on comfortability
                "icon": "a87" if env_data["is_comfort"] else "i10018",
                "index": 0
            }
        ]
    }

    requests.post(
        url=url,
        json=data,
        headers=headers
    )


# Ref: https://github.com/awslabs/amazon-timestream-tools/blob/master/sample_apps/python/CrudAndSimpleIngestionExample.py#L143-L169
def _convert_environment_data_to_records(env_data: Dict) -> Tuple[Dict, List[Dict]]:
    common_attributes = {
        "Dimensions": [
            {
                "Name": "device_id",
                "Value": env_data["device_id"]
            }
        ],
        "Time": env_data["timestamp"],
        "TimeUnit": "SECONDS"
    }

    humidity = {
        "MeasureName": "humidity",
        "MeasureValue": str(env_data["humidity"]),
        "MeasureValueType": "BIGINT",
    }

    illuminance = {
        "MeasureName": "illuminance",
        "MeasureValue": str(env_data["illuminance"]),
        "MeasureValueType": "BIGINT",
    }

    motion = {
        "MeasureName": "motion",
        "MeasureValue": str(env_data["motion"]),
        "MeasureValueType": "BIGINT",
    }

    is_moved = {
        "MeasureName": "is_moved",
        "MeasureValue": str(env_data["is_moved"]),
        "MeasureValueType": "BOOLEAN",
    }

    temperature = {
        "MeasureName": "temperature",
        "MeasureValue": str(env_data["temperature"]),
        "MeasureValueType": "DOUBLE",
    }

    discomfort_index = {
        "MeasureName": "discomfort_index",
        "MeasureValue": str(env_data["discomfort_index"]),
        "MeasureValueType": "DOUBLE",
    }

    is_comfort = {
        "MeasureName": "is_comfort",
        "MeasureValue": str(env_data["is_comfort"]),
        "MeasureValueType": "BOOLEAN",
    }

    records = [humidity, illuminance, motion, is_moved,
               temperature, discomfort_index, is_comfort]

    return common_attributes, records


# Ref: https://github.com/awslabs/amazon-timestream-tools/blob/master/sample_apps/python/CrudAndSimpleIngestionExample.py#L171-L178
def _put_environment_data(database_name: str, table_name: str, common_attributes: Dict, records: List[Dict]):
    session = boto3.Session()
    client = session.client(
        "timestream-write",
        config=Config(
            read_timeout=20,
            max_pool_connections=5000,
            retries={"max_attempts": 10}
        )
    )

    try:
        client.write_records(
            DatabaseName=database_name, TableName=table_name,
            CommonAttributes=common_attributes, Records=records
        )
    except client.exceptions.RejectedRecordsException as err:
        _print_rejected_records_exceptions(err)
        sys.exit(1)


# Ref: https://github.com/awslabs/amazon-timestream-tools/blob/master/sample_apps/python/CrudAndSimpleIngestionExample.py#L284-L289
def _print_rejected_records_exceptions(err):
    print("RejectedRecords: ", err)
    for rr in err.response["RejectedRecords"]:
        print("Rejected Index " + str(rr["RecordIndex"]) + ": " + rr["Reason"])
        if "ExistingVersion" in rr:
            print("Rejected record existing version: ", rr["ExistingVersion"])


def _calc_discomfort_index(temperature: int, humidity: float) -> float:
    # Ref: https://kenchiku-setsubi.biz/fukaishisu/
    discomfort_index = 0.81 * temperature + 0.01 * \
        humidity * (0.99 * temperature - 14.3) + 46.3

    return discomfort_index


def _add_discomfort_index_to_environment_data(env_data: Dict) -> Dict:
    # Calculate discoomfort index
    temperature = env_data["temperature"]
    humidity = env_data["humidity"]
    discomfort_index = _calc_discomfort_index(temperature, humidity)

    env_data["discomfort_index"] = discomfort_index
    # Judge whether this environment is comfortable or not(Ref: https://kenchiku-setsubi.biz/fukaishisu/)
    env_data["is_comfort"] = discomfort_index >= 60 and discomfort_index <= 75

    return env_data


# Ref: https://github.com/awslabs/amazon-timestream-tools/blob/master/sample_apps/python/QueryExample.py#L267-L273
def _add_motion_sensor_detection_result_to_environment_data(database_name: str, table_name: str, env_data: Dict) -> Dict:
    # Get latest data of the motion sensor in the Timestream
    session = boto3.Session()
    query_client = session.client("timestream-query")

    get_latest_result = f"""
        SELECT measure_value::bigint
        FROM \"{database_name}\".\"{table_name}\"
        WHERE measure_name = 'motion'
        AND time > ago(10m)
        ORDER BY time DESC
        LIMIT 1
    """

    try:
        latest_result = query_client.query(QueryString=get_latest_result)
    except ClientError as err:
        # If there is no sensor data, "is_moved" will be False because there is no evidence of moving
        if "Column 'measure_value::bigint' does not exist" in err.response["Error"]["Message"]:
            env_data["is_moved"] = False
            return env_data
        raise err
    except Exception as err:
        print("Exception while running query:", err)
        sys.exit(1)

    # If there is no data in last 10 minutes, "is_moved" will be False because there is no evidence of moving
    if not latest_result["Rows"]:
        env_data["is_moved"] = False
        return env_data

    latest_data = int(latest_result["Rows"][0]["Data"][0]["ScalarValue"])
    current_data = env_data["motion"]

    # If current data is greater than latest data in the Timestream, "is_moved" will be True because this event indicates that the sensor detect any motions
    env_data["is_moved"] = True if current_data > latest_data else False
    return env_data


def main(event, context):
    print("Get environment variables")
    service = os.environ["SERVICE"]
    stage = os.environ["STAGE"]

    print("Get API Token for operating 'Nature Remo'")
    secret_name = "{}-{}-NatureRemo".format(service, stage)
    secret = _get_secrets(secret_name)
    device_id = secret["DEVICE_ID"]
    token = secret["TOKEN"]

    print("Get device data from 'Nature Remo'")
    device_data = _get_device_data(token, device_id)

    print("Get API Token and URL for operating 'LaMetric Time'")
    secret_name = "{}-{}-LaMetricTime".format(service, stage)
    secret = _get_secrets(secret_name)
    token = secret["TOKEN"]
    url = secret["URL"]

    print("Send environment data to 'LaMetric Time'")
    env_data = _extract_environment_data(device_data)
    env_data = _add_discomfort_index_to_environment_data(env_data)
    _post_environment_data(token, url, env_data)

    print("Put environment data to 'Amazon Timestream'")
    database_name = "{}-{}".format(service, stage)
    table_name = "EnvironmentData"
    env_data = _add_motion_sensor_detection_result_to_environment_data(
        database_name, table_name, env_data)
    common_attributes, records = _convert_environment_data_to_records(env_data)
    _put_environment_data(database_name, table_name,
                          common_attributes, records)
